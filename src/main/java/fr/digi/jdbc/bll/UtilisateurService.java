package fr.digi.jdbc.bll;

import fr.digi.jdbc.bo.Utilisateur;
import fr.digi.jdbc.dal.DAOFactory;
import fr.digi.jdbc.dal.IUtilisateurDAO;

import java.sql.SQLException;

public final class UtilisateurService {
	
	private static UtilisateurService single;
	
	private UtilisateurService() {}
	
	public static UtilisateurService getSingle() {
		if (null==single) {
			single = new UtilisateurService();
		}
		return single;
	}
	
	public Utilisateur authentification( String login, String motDePasse ) throws SQLException {
		IUtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
		return dao.authentification( login, motDePasse );
	}
	
	public Utilisateur authentificationSecurisee( String login, String motDePasse ) throws SQLException {
		IUtilisateurDAO dao = DAOFactory.getUtilisateurDAO();
		return dao.authentificationSecurisee( login, motDePasse );
	}
	
}
