package fr.digi.jdbc.bo;

import java.io.Serializable;

public class Utilisateur implements Serializable {
	
	private Long id;
	private String nom;
	private String login;
	private String motDePasse;
	
	public Utilisateur() {
	}
	
	public Utilisateur( String nom, String login, String motDePasse ) {
		this.nom = nom;
		this.login = login;
		this.motDePasse = motDePasse;
	}
	
	public Utilisateur( Long id, String nom, String login, String motDePasse ) {
		this.id = id;
		this.nom = nom;
		this.login = login;
		this.motDePasse = motDePasse;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void setNom( String nom ) {
		this.nom = nom;
	}
	
	public String getLogin() {
		return login;
	}
	
	public void setLogin( String login ) {
		this.login = login;
	}
	
	public String getMotDePasse() {
		return motDePasse;
	}
	
	public void setMotDePasse( String motDePasse ) {
		this.motDePasse = motDePasse;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Utilisateur{" );
		sb.append( "id=" ).append( id );
		sb.append( ", nom='" ).append( nom ).append( '\'' );
		sb.append( ", login='" ).append( login ).append( '\'' );
		sb.append( ", motDePasse='" ).append( motDePasse ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
