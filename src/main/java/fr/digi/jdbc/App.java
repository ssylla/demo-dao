package fr.digi.jdbc;

import fr.digi.jdbc.bll.FournisseurService;
import fr.digi.jdbc.bll.UtilisateurService;
import fr.digi.jdbc.bo.Fournisseur;
import fr.digi.jdbc.bo.Utilisateur;

import java.sql.SQLException;
import java.util.Scanner;

public class App {
	
	public static void main( String[] args ) {
		
		authentification();
	}
	public static void authentification() {
		Scanner sc = new Scanner( System.in );
		System.out.println("Bienvenue dans mon App...");
		System.out.println("Merci de vous identifier");
		System.out.print("Login: ");
		String login = sc.nextLine();
		System.out.print("Mot de passe: ");
		String motDePasse = sc.nextLine();
		try {
			UtilisateurService service = UtilisateurService.getSingle();
			
			Utilisateur utilisateur1 = service.authentificationSecurisee( login, motDePasse );
			if (null != utilisateur1) {
				System.out.printf("Bienvenue en mode sécurisé, %s", utilisateur1.getNom());
			} else {
				System.out.println("Erreur d'identification en mode sécurisé");
			}
			
			Utilisateur utilisateur = service.authentification( login, motDePasse );
			if (null != utilisateur) {
				System.out.printf("Bienvenue, %s", utilisateur.getNom());
			} else {
				System.out.println("Erreur d'identification");
			}
		} catch ( SQLException throwables ) {
			System.err.println(throwables.getMessage());
		}
	}
	
	public static void ajoutFournisseur() {
		//FournisseurService service = new FournisseurService(); ==> Couplage fort
		//FournisseurService service = FournisseurService.getSingle(); ==> Couplage faible
		
		FournisseurService service = FournisseurService.getSingle();
		try {
			service.create( new Fournisseur( "MTC" ) );
		} catch ( SQLException e ) {
			System.err.println(e.getMessage());
		}
	}
	
}
