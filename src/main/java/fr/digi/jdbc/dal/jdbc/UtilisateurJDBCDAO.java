package fr.digi.jdbc.dal.jdbc;

import fr.digi.jdbc.bo.Utilisateur;
import fr.digi.jdbc.dal.IUtilisateurDAO;

import java.sql.*;
import java.util.Set;

public class UtilisateurJDBCDAO implements IUtilisateurDAO {
	
	private static final String AUTHENT_QUERY = "SELECT * FROM utilisateur WHERE LOGIN ='%s' AND MOT_DE_PASSE = '%s'";
	private static final String SECURED_AUTHENT_QUERY = "SELECT * FROM utilisateur WHERE LOGIN = ? AND MOT_DE_PASSE = ?";
	
	@Override
	public void create( Utilisateur o ) throws SQLException {
	
	}
	
	@Override
	public Utilisateur findById( Long aLong ) {
		return null;
	}
	
	@Override
	public Set<Utilisateur> findAll() {
		return null;
	}
	
	@Override
	public void update( Utilisateur o ) {
	
	}
	
	@Override
	public void delete( Utilisateur o ) {
	
	}
	
	@Override
	public void deleteById( Long aLong ) {
	
	}
	
	@Override
	public Utilisateur authentification( String login, String motDePasse ) throws SQLException {
		Connection connection = ConnectionDB.getSingle().getConnection();
		Utilisateur utilisateur = null;
		try ( Statement st = connection.createStatement() ) {
			System.out.println( String.format( AUTHENT_QUERY, login, motDePasse ) );
			try ( ResultSet rs = st.executeQuery( String.format( AUTHENT_QUERY, login, motDePasse ) ) ) {
				if ( rs.next() ) {
					utilisateur = new Utilisateur( rs.getLong( "ID" ), rs.getString( "NOM" ), rs
							.getString( "LOGIN" ), rs.getString( "MOT_DE_PASSE" ) );
				}
			}
		}
		return utilisateur;
	}
	
	@Override
	public Utilisateur authentificationSecurisee( String login, String motDePasse ) throws SQLException {
		Connection connection = ConnectionDB.getSingle().getConnection();
		Utilisateur utilisateur = null;
		try ( PreparedStatement pst = connection.prepareStatement(SECURED_AUTHENT_QUERY ) ) {
			pst.setString( 1, login );
			pst.setString( 2, motDePasse );
			try ( ResultSet rs = pst.executeQuery() ) {
				System.out.println(pst);
				if ( rs.next() ) {
					utilisateur = new Utilisateur( rs.getLong( "ID" ), rs.getString( "NOM" ), rs
							.getString( "LOGIN" ), rs.getString( "MOT_DE_PASSE" ) );
				}
			}
		}
		return utilisateur;
	}
}
