package fr.digi.jdbc.dal;

import fr.digi.jdbc.bo.Utilisateur;

import java.sql.SQLException;

public interface IUtilisateurDAO extends IDAO<Utilisateur, Long> {
	
	Utilisateur authentification(String login, String motDePasse) throws SQLException;
	Utilisateur authentificationSecurisee(String login, String motDePasse) throws SQLException;
}
