package fr.digi.jdbc.dal;

import fr.digi.jdbc.bo.Fournisseur;

public interface IFournisseurDAO extends IDAO<Fournisseur, Long> {}
